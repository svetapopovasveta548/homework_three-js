// 1) Циклы нужны для того чтобы не дублировать код, в цикле мы можем написать один раз условие и проверять его до тех пор пока оно true


// 2) Использую while и for одинаково. В while преимущество в то что она проще для восприятия, а в for данные можно записать в один ряд это быстрее. do while использую когда хочу, чтобы итерация выполнилась хотя бы один раз, если даже условие false


// 3)Неявное преобразовывается автоматически.например через операторы, а явное вручную , через функции: Boolean(), Number(), String() или унарный плюс


let numberInfo = +prompt("Enter your number");

while (!Number.isInteger(numberInfo)) {
  numberInfo = +prompt("Enter your number again");
}

for (let i = 1; i <= numberInfo; i++) {

  if (i % 5 == 0) {
    console.log(i);
  } else if (5 > numberInfo) {
    console.log("Sorry, no numbers");
  }
}